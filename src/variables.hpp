/***************************************************************************
 *            variables.hpp
 *
 *  Pá leden 02 03:11:37 2015
 *  Copyright  2015  venca
 *  <vaclav@linux.com>
 ****************************************************************************/
/*
 * variables.hpp
 *
 * Copyright (C) 2015 - venca
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VAR_H
#define VAR_H
//system includes
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
//#include <dirent.h>

using namespace std;
using namespace sf;

#endif // VAR_H