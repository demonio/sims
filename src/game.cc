// game.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
#include "game.h"

extern RenderWindow window;
vector< buttons > buts;
vector< buttons > menub;
extern vector< character > players;
extern int w_height;
extern int w_width;
extern int mx;
extern int my;
extern int state;
extern Color Red;
extern Color White;
extern Font cfont;
extern bool reconf;
vector<vector < segment > > seg;
vector<vector < segment > > object;
View view;
int px = 640;
int py = 640;
string menus;
string floors = "none";
string objects = "nic";
int pohyb = 0;

void menu_change (string menu){
	// Lepsi if jak switch ( nemusim pouzivat cisla)
	if (menu == "Floor"){
		menub.clear();
		menub.resize(5);
		menub[0].set_values (px-(w_width/2), py, "grass");
		menub[1].set_values (px-(w_width/2), py+50, "dirt");
		menub[2].set_values (px-(w_width/2), py-50, "floor1");
		menub[3].set_values (px-(w_width/2), py-100, "floor2");
		menub[4].set_values (px-(w_width/2), py+100, "water");
		//TODO
	} else if (menu == "Object")	{
		menub.clear();
		menub.resize(5);
		menub[0].set_values (px-(w_width/2), py, "none");
		menub[1].set_values (px-(w_width/2), py+50, "table");
		menub[2].set_values (px-(w_width/2), py-50, "wall1");
		menub[3].set_values (px-(w_width/2), py-100, "wall2");
		menub[4].set_values (px-(w_width/2), py+100, "chair");
	} else {
		menub.clear();
	}
	menus = menu;
}

void game_load (bool reload){
	if (reload){
		//202 141
		view.setCenter(px, py);
		view.setSize(w_width, w_height);
		seg.resize( 33 , vector< segment >( 32 ) );
		object.resize( 33 , vector< segment >( 32 ) );
		for (int i = 0; i < 33; i += 1){
			for (int k = 0; k < 32; k += 1){
				if(i<32){
					seg[i][k].set_sprite ("grass");}
				else
					seg[i][k].set_sprite ("road");
				seg[i][k].sprite.setPosition(i*64,k*64);
			}
		}
		for (int i = 0; i < 32; i += 1){
			for (int k = 0; k < 32; k += 1){
				object[i][k].set_sprite ("none");
				object[i][k].sprite.setPosition(i*64,k*64);
			}
		}
		buts.clear();
		buts.resize(2);//TODO
		buts[0].set_values (px-(w_width/2), py+(w_height/2), "Floor");
		buts[1].set_values (px-(w_width/2), py+(w_height/2), "Object");
	}
}

bool game_update (Event event){
	sf::Vector2u wsize = window.getSize();
	w_width = wsize.x;
	w_height = wsize.y;
	view.setSize(float(wsize.x), float(wsize.y));
	if (pohyb == 0 and (int) object.capacity() == 33 ){
		if (event.key.code == Keyboard::A and px > 0){
			if(object[(px - 64)/64][py/64].walkable == true){
				view.move(-64, 0);
				px -= 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::W and py > 0){
			if(object[(px)/64][(py-64)/64].walkable == true){
				view.move(0, -64);
				py -= 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::D and px < ((33*64)-64)){
			if(object[(px + 64)/64][py/64].walkable == true){
				view.move(64, 0);
				px += 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::S and py < ((32*64)-64)){
			if(object[(px)/64][(py+64)/64].walkable == true){
				view.move(0, 64);
				py += 64;
				pohyb = 2;
			}
		}
	} else {
		if (pohyb > 0)
			pohyb--;
	}
	//if (event.key.code == Keyboard::Add){
	//	view.zoom(0.90f);
	//}
	//if (event.key.code == Keyboard::Subtract ){
	//	view.zoom(1.10f);
	//}
	players[0].head.setPosition(px, py);
	players[0].nose.setPosition(px+20, py+19);
	players[0].hair.setPosition(px+2, py-9);
	players[0].browl.setPosition(px+24, py+11);
	players[0].browr.setPosition(px+20, py+11);
	players[0].eyel.setPosition(px+27, py+14);
	players[0].eyer.setPosition(px+17, py+14);
	players[0].mounth.setPosition(px+19, py+30);
	if ((int) seg.capacity() == 33){
		buts[0].show.setPosition(float(px-(view.getSize().x/2)+10), float(py+(view.getSize().y/2)-30));
		buts[1].show.setPosition(float(px-(view.getSize().x/2)+110), float(py+(view.getSize().y/2)-30));
		for (unsigned i=0; i<buts.size(); i++){
			buts[i].x = buts[i].show.getPosition().x;
			buts[i].y = buts[i].show.getPosition().y;
		}	
	}
	if (floors != "none"){
		if (event.key.code == sf::Keyboard::Escape){
			floors = "none";
		}
		if (event.type == Event::MouseButtonPressed){
			if (int((int(my) + (py - wsize.y/2))/64) < 33)
				seg[int((int(mx) + (px - wsize.x/2))/64)][int((int(my) + (py - wsize.y/2))/64)].set_sprite (floors);
		}
	}
	if (objects != "nic"){
		if (event.key.code == sf::Keyboard::Escape){
			objects = "nic";
		}
		if (event.type == Event::MouseButtonPressed){
			if (int((int(my) + (py - wsize.y/2))/64) < 32)
				object[int((int(mx) + (px - wsize.x/2))/64)][int((int(my) + (py - wsize.y/2))/64)].set_sprite (objects);
				if (objects == "wall1" or objects == "wall2"){
					object[int((int(mx) + (px - wsize.x/2))/64)][int((int(my) + (py - wsize.y/2))/64)].walkable = false;
				} else {
					object[int((int(mx) + (px - wsize.x/2))/64)][int((int(my) + (py - wsize.y/2))/64)].walkable = true;
				}
		}
	}
	if ((int) seg.capacity() < 33)
		return false;
	for (unsigned i=0; i<buts.size(); i++)
		if (buts[i].check(int(mx) + (px - wsize.x/2),int(my) + (py - wsize.y/2)))
			buts[i].show.setColor(Color(Red));
		else
			buts[i].show.setColor(Color(White));
	if (event.type == Event::MouseButtonPressed){
		for (unsigned i=0; i<buts.size(); i++)
			if (buts[i].check(int(mx) + (px - wsize.x/2),int(my) + (py - wsize.y/2)))
				if (menus == string(buts[i].show.getString()))
					menu_change ("none");
				else
					menu_change (string(buts[i].show.getString()) );
	}
	//sub buttons
	if ((int) menub.capacity() == 0)
		return false;
	menu_change (menus);
	for (unsigned i=0; i<menub.size(); i++)
		if (menub[i].check(int(mx) + (px - wsize.x/2),int(my) + (py - wsize.y/2)))
			menub[i].show.setColor(Color(Red));
		else
			menub[i].show.setColor(Color(White));
	if (event.type == Event::MouseButtonPressed){
		for (unsigned i=0; i<menub.size(); i++)
			if (menub[i].check(int(mx) + (px - wsize.x/2),int(my) + (py - wsize.y/2))){
				if (menus == "Floor"){
					floors = string(menub[i].show.getString());
					menu_change ("none");
				}
				else if (menus == "Object"){
					objects = string(menub[i].show.getString());
					menu_change ("none");
				}
		}
	}
	return false;
}

void game_draw (){
	if ((int) seg.capacity() == 33){
		window.setView(view);
		for (int i = 0; i < 33; i += 1){
			for (int k = 0; k < 32; k += 1){
				window.draw(seg[i][k].sprite);
			}
		}
		for (int i = 0; i < 32; i += 1){
			for (int k = 0; k < 32; k += 1){
				window.draw(object[i][k].sprite);
			}
		}
		for (unsigned i=0; i<buts.size(); i++)
			window.draw(buts[i].show);
		for (unsigned i=0; i<menub.size(); i++)
			window.draw(menub[i].show);
		window.draw(players[0].head);;
		window.draw(players[0].nose);
		window.draw(players[0].hair);
		window.draw(players[0].browl);
		window.draw(players[0].browr);
		window.draw(players[0].eyel);
		window.draw(players[0].eyer);
		window.draw(players[0].mounth);
	}
	else
		reconf = true;
}